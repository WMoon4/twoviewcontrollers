//
//  InputFieldsViewController.swift
//  twoViewControllers
//
//  Created by V.K. on 2/19/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

protocol InputFieldsViewDelegate {
    func fieldsSet(name: String, email: String)
}

class InputFieldsViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    
    var delegate: InputFieldsViewDelegate?
    
    
    @IBAction func saveInputs(_ sender: Any) {
        delegate?.fieldsSet(name: nameField.text ?? "",
                            email: emailField.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
    
}
