//
//  MasterViewController.swift
//  twoViewControllers
//
//  Created by V.K. on 2/19/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class MasterViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case "showInputView":
            let destinationView = segue.destination as? InputFieldsViewController
            destinationView?.delegate = self
        default:
            break
        }
    }

}

extension MasterViewController: InputFieldsViewDelegate {
    func fieldsSet(name: String, email: String) {
        nameLabel.text = name
        emailLabel.text = email
    }
}

